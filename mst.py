import math
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree
from networkx.algorithms.traversal.depth_first_search import dfs_tree
import networkx as nx
from networkx.algorithms import bipartite
import matplotlib.pyplot as plt
import seaborn as sns
import copy
import random
import settings


def hamming_distance(seq1, seq2):
    """Simple hamming distance between two seqeuences"""
    distance = 0
    for nucl1, nucl2 in zip(seq1, seq2):
        if nucl1 != nucl2: distance += 1
    return distance / len(seq1)


def uncorrected_distance_matrix(seq_list):
    """Computes a distance matrix, which is a representation of a complete graph"""
    D = np.asarray([[hamming_distance(seq1, seq2) for seq2 in seq_list] for seq1 in seq_list])
    return D


def JC_distance_matrix(seq_list):
    """Jukes cantor distance correction on the distance matrix"""
    # TODO: make sure the value within the log does not get below 0
    D = uncorrected_distance_matrix(seq_list)
    D[D > 0.75] = 0.75
    return -0.75 * np.log(1 - (D * 4 / 3))


def get_mst(seq_list):
    """Computes the MST using scipy

        Parameters:
        ----------
        sequences
            a list of sequences as strings.

        Returns:
        --------
        The mininimum spanning tree as a matrix representation.
        """
    D = JC_distance_matrix(seq_list)
    # mst_matrix = minimum_spanning_tree(D)
    # mst_matrix.toarray().astype(float)
    return minimum_spanning_tree(D)

def calc_number_cuts(mst_matrix, k=1):
    """"Calculating the number of cuts, maybe through:
    1. average distances (could also be done on the complete distance matrix.
    2. the distribution of distances
    3. some other factor influencing the accuracy, such as the quality of the alignment.
    4. based on a clustering coefficient that gives info on the expected number of clusters in the graph.
    Either based on the mst_matrix, or the complete distance matrix.

    any of these functions should be scalable, and experimented with with actual data

    Something like average distance should ofc also be a function of sequence lenght and number of sequences.

    If you always want to obtain the same accuracy:
    Higher sequence length ==> more cuts
    Higher average distance ==> less cuts
    Higher number of sequences ==> (more free parameters, more topologies, less accurate) ==> less cuts.

    We can test this by making a graph of the involved parameters against the Robison fauld distance.
    """
    #number_of_sequences = mst_matrix.shape[0]
    #n = int(0.05*number_of_sequences) + 1
    return 2

def draw_graph_longest_path(mst_matrix, seq_dict, longest_path):
    all_colors = {}
    mst_matrix = (mst_matrix * 1/(mst_matrix + 0.01))
    G = nx.from_numpy_matrix(mst_matrix, parallel_edges=False, create_using=None)
    G = nx.relabel.relabel_nodes(G, {i: list(seq_dict.keys())[i] for i in range(0, len(seq_dict))})
    for first, second in G.edges():
        G[first][second]["color"] = "black"

    for i in range(0, len(longest_path) - 1):
        G[longest_path[i]][longest_path[i + 1]]["color"] = "red"
    colors = [G[u][v]["color"] for u,v in G.edges()]
    remapping = {node : node[5:]  for node in G.nodes()}
    G = nx.relabel.relabel_nodes(G, remapping)
    for first, second in G.edges():
        G[first][second]["weight"] = 1 - G[first][second]["weight"]

    #nx.draw(G, with_labels=False, edge_color = colors, node_size=10, width=0.5, edgecolors='black', node_color='lightgray')
    nx.draw(G, with_labels=True, edge_color = colors, node_color = "white")
    plt.show()


def draw_graph(mst_matrix, seq_dict):
    mst_matrix = mst_matrix * 1/(mst_matrix + 0.01)
    G = nx.from_numpy_matrix(mst_matrix, parallel_edges=False, create_using=None)
    G = nx.relabel.relabel_nodes(G, {i: list(seq_dict.keys())[i] for i in range(0, len(seq_dict))})
    G = nx.relabel.relabel_nodes(G, {node : node[5:]  for node in G.nodes()})
    for first, second in G.edges():
        G[first][second]["weight"] = 1 - G[first][second]["weight"]

    nx.draw(G, with_labels=True, node_color = "white")
    plt.show()

def find_longest_path(G): # works only on tree, not graphs
    '''
    it makes two DFSearches to find the longest path in the tree
    '''
    random_node = random.choice(list(G.nodes()))
    distances = {random_node: 0}
    for edge in nx.dfs_edges(G, source=random_node):
        (parent, child) = edge
        distances[child] = distances[parent] + 1
    source = max(distances, key=lambda k: distances[k])

    distances = {source : 0}
    for edge in nx.dfs_edges(G, source=source):
        (parent, child) = edge
        distances[child] = distances[parent] + 1
    target = max(distances, key=lambda k: distances[k])

    path = list(nx.shortest_path(G, source=source, target=target))
    return path

def compute_pk_values(G, longest_path):
    '''
    compute pk values only along the longest path
    '''
    pk_value_nodes = {longest_path[0] : 1, longest_path[-1] : len(G.nodes())} # first one is a leaf.
    pk_value_weights = {longest_path[0] : 0, longest_path[-1] : G.size(weight="weight")} # first one is a leaf.
    for i in range(1,len(longest_path)-1):
        G_temp = G.copy()
        G_temp.remove_edge(longest_path[i], longest_path[i + 1])
        dfs_edges = nx.dfs_edges(G_temp, longest_path[i])
        pk_value_nodes[longest_path[i]] =  len(list(dfs_edges)) + 1
        pk_value_weights[longest_path[i]] = sum([G.get_edge_data(edge[0],edge[1])['weight'] for edge in dfs_edges])
    return pk_value_nodes

def cut_mst_longest_branches(n, mst_matrix, seq_dict = {}):
    """Cuts the MST in into mutually disjoint sets

        Parameters:
        ----------
        mst_matrix
            The mininimum spanning tree as a matrix representation.
        n
            The number of vertices that needs to be cut.

        Returns:
        --------
        forest
            A list of matrices representing the disjoint trees.
        cut_vertices
            A list of vertices that have been cut
        """
    mst_matrix_sparse = mst_matrix
    mst_matrix = mst_matrix.toarray().astype(float)

    if (n != 1):
        n = n - 1 # Want number of cuts
        sorted_indices = np.unravel_index(np.argsort(mst_matrix, axis=None), mst_matrix.shape)
        cut_vertices = {(ind1, ind2): mst_matrix[(ind1, ind2)] for ind1, ind2 in zip(sorted_indices[0][-n:], sorted_indices[1][-n:])}
        cut_mst_matrix = copy.deepcopy(mst_matrix)
        cut_mst_matrix[sorted_indices[0][-n:], sorted_indices[1][-n:]] = 0
        G = nx.from_numpy_matrix(cut_mst_matrix, parallel_edges=False, create_using=None)
        forest = [G.subgraph(c).copy() for c in nx.connected_components(G)]

        if settings.PLOT_DEGREE:
            G = nx.from_numpy_matrix(mst_matrix, parallel_edges=False, create_using=None)
            plot_degree_distribution(G)
        if seq_dict and settings.PLOT_NETWORK:
            draw_graph(mst_matrix, seq_dict)
            draw_graph(cut_mst_matrix, seq_dict)

        if settings.PLOT_DISTRIBUTION:
            plot_subset_size_distribution(forest)
            plot_edge_distribution(mst_matrix_sparse, cut_vertices)
        return forest, cut_vertices
    else:
        G = nx.from_numpy_matrix(mst_matrix, parallel_edges=False, create_using=None)
        return [G],0


def cut_mst_longest_path(n, mst_matrix, seq_dict = {}):
    """Cuts the MST in into mutually disjoint sets

        Parameters:
        ----------
        mst_matrix
            The mininimum spanning tree as a matrix representation.
        n
            The number of vertices that needs to be cut.

        Returns:
        --------
        forest
            A list of matrices representing the disjoint trees.
        cut
            A list of edges that have been cut
        """

    mst_matrix_sparse = mst_matrix

    mst_matrix = mst_matrix.toarray().astype(float)

    G = nx.from_numpy_matrix(mst_matrix, parallel_edges=False, create_using=None)
    G = nx.relabel.relabel_nodes(G, {i: list(seq_dict.keys())[i] for i in range(0, len(seq_dict))})


    longest_path = find_longest_path(G)
    nr_nodes = len(G.nodes())

    total_size =  G.size(weight="weight")
    partitions = n
    optimal_number = nr_nodes/partitions
    optimal_size = total_size/partitions

    pk_values = compute_pk_values(G, longest_path)

    if settings.print_cut_info:
        print (pk_values)
        print (longest_path)

    optimal_one = optimal_number
    cut = []
    for i in range(1,len(longest_path)- 1):
        if pk_values[longest_path[i]] >= optimal_number:
            if (optimal_number - pk_values[longest_path[i - 1]]) < (pk_values[longest_path[i]] -  optimal_number):
                cut.append((longest_path[i - 1], longest_path[i]))
            else:
                cut.append((longest_path[i], longest_path[i +  1]))
            optimal_number += optimal_one


    #removing duplicates
    cut = list(set(cut))
    if settings.print_cut_info: print ("cut: ", cut)

    cut_mst_matrix = copy.deepcopy(mst_matrix)
    sorted_indices = np.unravel_index(np.argsort(mst_matrix, axis=None), mst_matrix.shape)

    for edge in cut:
        G.remove_edge(edge[0], edge[1])

    cut_mst_matrix = nx.to_numpy_array(G)

    forest = [G.subgraph(c).copy() for c in nx.connected_components(G)]


    if settings.PLOT_DEGREE:
        G = nx.from_numpy_matrix(mst_matrix, parallel_edges=False, create_using=None)
        plot_degree_distribution(G)
    if seq_dict and settings.PLOT_NETWORK:
        draw_graph(mst_matrix, seq_dict)
        draw_graph_longest_path(mst_matrix, seq_dict, longest_path)
        draw_graph(cut_mst_matrix, seq_dict)

    if settings.PLOT_DISTRIBUTION: plot_subset_size_distribution(forest)

    return forest,cut

def cut_mst_downstream_nodes(n, mst_matrix, seq_dict = {}):
    mst_matrix_sparse = mst_matrix
    mst_matrix = mst_matrix.toarray().astype(float)

    G = nx.from_numpy_matrix(mst_matrix, parallel_edges=False, create_using=None)
    G = nx.relabel.relabel_nodes(G, {i: list(seq_dict.keys())[i] for i in range(0, len(seq_dict))})
    partitions = n

    S = copy.deepcopy(G)
    pk_values = {taxon:1 for taxon in S.nodes}
    nodes_list = list(S.degree())
    while (len(nodes_list) > 1):
        nodes_list.sort(key = lambda x: x[1])
        i = 0
        leaf = True
        while leaf:
            taxon_leaf,degree = nodes_list[i]
            if ((degree == 1) and (len(list(S[taxon_leaf])) == 1)):
                taxon_inner = list(S[taxon_leaf])[0]
                pk_values[taxon_inner] += pk_values[taxon_leaf]
                S.remove_edge(taxon_leaf, taxon_inner)
                S.remove_node(taxon_leaf)
                i += 1
            else:
                leaf = False
                nodes_list = list(S.degree())

    if settings.print_cut_info: print (pk_values)

    cuts = []
    for i in range(partitions-1): #want number of cuts
        u = max(pk_values,key=pk_values.get)
        neighbors = list(G.neighbors(u))
        neighbors_degree = {node:pk_values[node] for node in neighbors}
        cut = False
        while not cut:
            v = max(neighbors_degree,key=pk_values.get)
            if (([u,v] in cuts) or ([v,u] in cuts)):
                neighbors_degree.pop(v)
            else:
                cut = True
                cuts.append([u,v])
        pk_values[u] -= pk_values[v]

    if settings.print_cut_info: print ("cut: ", cuts)

    for edge in cuts:
        G.remove_edge(edge[0], edge[1])
    cut_mst_matrix = nx.to_numpy_array(G)
    forest = [G.subgraph(c).copy() for c in nx.connected_components(G)]

    if settings.PLOT_DEGREE:
        G = nx.from_numpy_matrix(mst_matrix, parallel_edges=False, create_using=None)
        plot_degree_distribution(G)
    if seq_dict and settings.PLOT_NETWORK:
        draw_graph(mst_matrix, seq_dict)
        draw_graph(cut_mst_matrix, seq_dict)

    if settings.PLOT_DISTRIBUTION:
        plot_subset_size_distribution(forest)
        plot_edge_distribution(mst_matrix_sparse, cut_vertices)

    return forest,cuts

def get_super_set(seq_dict, n = -1):
    seq_list = list(seq_dict.values())
    mst_matrix = get_mst(seq_list)

    if (n == -1):
        n = calc_number_cuts(mst_matrix)

    if settings.mst_cut == "longest_branch":
        forest, cut_vertices = cut_mst_longest_branches(n, mst_matrix, seq_dict)
    elif settings.mst_cut == "longest_path":
        forest, cut_vertices = cut_mst_longest_path(n, mst_matrix, seq_dict)
    else:
        forest, cut_vertices = cut_mst_downstream_nodes(n, mst_matrix, seq_dict)

    super_set = []
    for tree in forest:
        nodes = list(tree.nodes)
        if settings.mst_cut == "longest_branch": nodes = [list(seq_dict.keys())[nodes[i]]  for i in range(len(tree))]
        sub_seq_dict = {node: seq_dict[node] for node in nodes}
        super_set.append(sub_seq_dict)
    return super_set

def plot_edge_distribution(mst_matrix, cut_vertices):
    data = mst_matrix.data
    cut_threshold = list(cut_vertices.values())[0]
    plt.axvspan(xmin=cut_threshold, xmax=max(data)+0.1, facecolor='white', alpha=0.5, label="cut edges", hatch='///',
                edgecolor="black")     # also vizualize the cut vertices. with dotted line.
    sns.distplot(data, color='grey', hist_kws={"edgecolor": 'white'})
    plt.legend(edgecolor="white")
    plt.xlim(xmin=0, xmax=max(data)+0.1)
    plt.xlabel("JC distance")
    plt.ylabel("Number of edges in MST")
    plt.show()

def plot_degree_distribution(G):
    sns.distplot(a=list(dict(G.degree).values()), color='grey', hist_kws={"edgecolor": 'white'})
    #plt.hist(list(dict(G.degree).values()), bins='auto', color="black", histtype='step')
    plt.xlim(xmin=0)
    plt.xlabel("Degree")
    plt.ylabel("Number of vertices in MST")
    plt.show()

def plot_subset_size_distribution(forest):
    data = [len(G.nodes) for G in forest]
    sns.distplot(data, color='grey', hist_kws={"edgecolor": 'white'})
    #plt.hist(list(dict(G.degree).values()), bins='auto', color="black", histtype='step')
    plt.xlim(xmin=0)
    plt.xlabel("Size of subset $|S_i|$")
    plt.ylabel("Number of subsets")
    plt.show()
