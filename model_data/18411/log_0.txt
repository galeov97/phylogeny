Partition 0: partition_0
Model: GTR+FO+G4m
Alignment sites / patterns: 29805 / 559
Gaps: 0.00 %
Invariant sites: 88.74 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: GAMMA (4 cats, mean),  alpha: 78.925140 (ML),  weights&rates: (0.250000,0.860768) (0.250000,0.959925) (0.250000,1.032840) (0.250000,1.146468)
Base frequencies (ML): 0.294505 0.192720 0.201176 0.311599
Substitution rates (ML): 1.059287 1.020411 0.825129 1.237099 1.032175 1.000000
