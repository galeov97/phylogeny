Partition 0: partition_0
Model: GTR+FO+G4m
Alignment sites / patterns: 2251 / 926
Gaps: 59.85 %
Invariant sites: 66.02 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: GAMMA (4 cats, mean),  alpha: 0.179220 (ML),  weights&rates: (0.250000,0.000238) (0.250000,0.022713) (0.250000,0.327299) (0.250000,3.649749)
Base frequencies (ML): 0.246350 0.257780 0.287780 0.208090
Substitution rates (ML): 0.947146 2.504725 1.088990 0.455171 5.690958 1.000000
