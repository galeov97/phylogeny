Partition 0: partition_0
Model: GTR+FO+G4m
Alignment sites / patterns: 596 / 419
Gaps: 21.77 %
Invariant sites: 38.42 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: GAMMA (4 cats, mean),  alpha: 0.406535 (ML),  weights&rates: (0.250000,0.017668) (0.250000,0.186621) (0.250000,0.738257) (0.250000,3.057454)
Base frequencies (ML): 0.215397 0.264281 0.250665 0.269657
Substitution rates (ML): 1.983761 3.127253 2.462328 0.692567 4.792354 1.000000
