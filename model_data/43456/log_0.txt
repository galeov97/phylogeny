Partition 0: partition_0
Model: GTR+FC
Alignment sites / patterns: 23633 / 9402
Gaps: 85.68 %
Invariant sites: 46.64 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: NONE
Base frequencies (empirical): 0.266766 0.188353 0.197339 0.347543
Substitution rates (ML): 1.277484 2.659666 1.229210 1.317214 2.439058 1.000000
