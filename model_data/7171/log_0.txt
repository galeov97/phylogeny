Partition 0: partition_0
Model: GTR+FO+G4m
Alignment sites / patterns: 1488 / 1328
Gaps: 19.43 %
Invariant sites: 23.19 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: GAMMA (4 cats, mean),  alpha: 0.474364 (ML),  weights&rates: (0.250000,0.028740) (0.250000,0.234851) (0.250000,0.800680) (0.250000,2.935729)
Base frequencies (ML): 0.206335 0.260196 0.316200 0.217269
Substitution rates (ML): 0.938482 2.837281 1.419086 0.691650 5.596761 1.000000
