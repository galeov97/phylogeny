Partition 0: partition_0
Model: GTR+FO+I+G4m
Alignment sites / patterns: 17454 / 1198
Gaps: 6.25 %
Invariant sites: 83.55 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: GAMMA (4 cats, mean),  alpha: 0.378565 (ML),  weights&rates: (0.250000,0.013747) (0.250000,0.165572) (0.250000,0.707003) (0.250000,3.113677)
Base frequencies (ML): 0.433439 0.092317 0.052685 0.421560
Substitution rates (ML): 3.574472 26.647407 8.580101 2.302651 55.974332 1.000000
