Partition 0: partition_0
Model: GTR+FO+I+G4m
Alignment sites / patterns: 658 / 61
Gaps: 0.22 %
Invariant sites: 80.40 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: GAMMA (4 cats, mean),  alpha: 0.251326 (ML),  weights&rates: (0.250000,0.002174) (0.250000,0.067649) (0.250000,0.504274) (0.250000,3.425902)
Base frequencies (ML): 0.311791 0.139692 0.152846 0.395671
Substitution rates (ML): 0.001000 134.305554 181.158407 41.686814 683.309896 1.000000
