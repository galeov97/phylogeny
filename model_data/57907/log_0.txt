Partition 0: partition_0
Model: GTR+FO+G4m
Alignment sites / patterns: 3400 / 2929
Gaps: 51.84 %
Invariant sites: 20.12 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: GAMMA (4 cats, mean),  alpha: 1.133413 (ML),  weights&rates: (0.250000,0.162779) (0.250000,0.513481) (0.250000,1.019013) (0.250000,2.304727)
Base frequencies (ML): 0.270274 0.261480 0.281938 0.186307
Substitution rates (ML): 2.252444 4.411499 1.587148 1.351962 4.801392 1.000000
