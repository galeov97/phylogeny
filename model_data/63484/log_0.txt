Partition 0: partition_0
Model: GTR+FO+G4m
Alignment sites / patterns: 4361 / 1278
Gaps: 9.13 %
Invariant sites: 75.28 %
Optimized model parameters:
Partition 0: partition_0
Rate heterogeneity: GAMMA (4 cats, mean),  alpha: 0.219577 (ML),  weights&rates: (0.250000,0.000983) (0.250000,0.045778) (0.250000,0.432801) (0.250000,3.520438)
Base frequencies (ML): 0.273354 0.193142 0.211414 0.322090
Substitution rates (ML): 1.081715 5.349282 1.042321 0.734908 5.405704 1.000000
