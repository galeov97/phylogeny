import numpy as np

def up_distance(seq_index, distance_matrix):
    return np.sum(distance_matrix[seq_index, :])

def hamming_distance(seq1, seq2):
    """Simple hamming distance between two seqeuences"""
    distance = 0
    for nucl1, nucl2 in zip(seq1, seq2):
        if nucl1 != nucl2: distance += 1
    return distance / len(seq1)

def uncorrected_distance_matrix(seq_list):
    """Computes a distance matrix, which is a representation of a complete graph"""
    D = np.asarray([[hamming_distance(seq1, seq2) for seq2 in seq_list] for seq1 in seq_list])
    return D
