#!/bin/bash

#SBATCH --mail-user=sjannalda@zedat.fu-berlin.de
#SBATCH --job-name=PhylogenyInference
#SBATCH --mail-type=end
#SBATCH --mem=100
#SBATCH --time=0-48:00:00
#SBATCH --qos=standard
#SBATCH --output=log/%x.%j.out

declare -a combinations
index=0
while read model
do
  while read length
  do
      combinations[$index]="$model $length"
      index=$((index + 1))
  done < "sequence_lengths.txt"
done < "models.txt"


parameters=(${combinations[${SLURM_ARRAY_TASK_ID}]})
model=${parameters[0]}
length=${parameters[1]}


python testing.py $model $length
