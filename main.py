import math, os, copy, random, sys
import matplotlib.pyplot as plt
import numpy as np
from Bio import SeqIO

from tree import Tree, Vertex
import mst
import stepwiseaddition
import merging
import robinson_foulds_distance
import settings

"""File contains workflow for phylogeny creation using the MST-based algorithm and simulated data"""

def OneModel(dir, length):
    # directories
    input_dir = 'model_data'
    estimated_dir = 'model_data'
    output_dir = 'output'

    # get actual newick
    my_tree = '/'.join([input_dir, dir, 'tree_best.newick'])
    with open(my_tree, "r") as file:
        actual_newick = file.readline()

    log = '/'.join([input_dir, dir, 'log_0.txt'])
    with open(log) as log_file:
        lines = log_file.readlines()
    model = lines[1].split(' ')[1].split('+')[0]

    # get base frequencies
    base_frequencies = lines[8].split(' ')[3:7]
    base_frequencies = [item.strip() for item in base_frequencies]

    # get substituatiom rates
    substitution_rates = lines[9].split(' ')[3:9]
    substitution_rates = [float(item.strip()) for item in substitution_rates]
    average_substitution_rates = np.mean(substitution_rates)

    # get sequences
    fastafile = output_dir + '/' + str(dir) + '/' + str(dir) + '_' + str(length) + '.fasta'
    seq_dict = {rec.id: str(rec.seq) for rec in SeqIO.parse(fastafile, "fasta")}

    return (seq_dict,actual_newick)

def create_phylogenetic_tree(seq_dict):
    """ The workflow of the phylogenetic tree creation using MST.

        Parameters:
        ----------
        seq_dict
            a dictionary of unique sequence names, and aligned sequences as strings.

        Returns:
        --------
        newick_format
            newick format of the tree topology with branch lenghts.

        """

    print("MST Starting...")
    super_set = mst.get_super_set(seq_dict)
    print("...MST Finishing")
    forest = set() # s/o forest gump for believing in us
    for sub_seq_dict in super_set:
        print("\tStepwise Starting...")
        sub_tree = stepwiseaddition.StepWiseAddition_MP(sub_seq_dict)
        print("\t\tNNI Starting...")
        PS_score = 0
        if len(sub_tree.nodes) > 2:
            PS_score = sub_tree.ComputeParsimonyScore()
        sub_tree.FromRootedtoUnrooted()
        if settings.PLOT_NNI and len(sub_tree.nodes) > 2:
            plot_nni = [PS_score]
            sub_tree = sub_tree.RoundsNNI_MP(PS_score, plot_nni)
        else:
            sub_tree = sub_tree.RoundsNNI_MP(PS_score)
        print("\t\t...NNI Finishing")
        #sub_tree.FromUnrootedtoRooted()
        forest.add(sub_tree)
        print("\t...Stepwise Finishing")

    print("Merging Starting...")
    final_tree = merging.merging(forest)
    print("...Merging Finishing")

    print("NNI Whole Tree Starting...")
    final_tree.FromUnrootedtoRooted()
    PS_score = final_tree.ComputeParsimonyScore()
    final_tree.FromRootedtoUnrooted()
    if settings.PLOT_NNI:
        plot_nni = [PS_score]
        final_tree = final_tree.RoundsNNI_MP(PS_score, plot_nni)
    else:
        final_tree = final_tree.RoundsNNI_MP(PS_score)
    final_tree.FromUnrootedtoRooted()
    final_tree.MidPointRooting(seq_dict=seq_dict)

    print("...NNI Whole Tree Finishing")
    newick_format = final_tree.ComputeNewickFormat()

    if settings.PLOT_TREE: final_tree.Visualize()

    return newick_format

if __name__ == '__main__':
    settings.init()
    model = sys.argv[1]
    length = int(sys.argv[2])

    seq_dict,actual_newick = OneModel(model,length)


    if (os.path.exists(os.path.join("results","%s" %model, str(length) + ".txt"))):
        with open(os.path.join("results", model, str(length) + ".txt"), "r") as file:
            newick_format = file.readline()
    else:
        newick_format = create_phylogenetic_tree(seq_dict)

        if (not os.path.isdir(os.path.join("results","%s" %model))):
            os.mkdir(os.path.join("results","%s" %model))

        with open(os.path.join("results","%s" %model, str(length) + ".txt"), "w+") as file:
            file.write(newick_format)

    print("Model:%s, RF:%f" %(model,robinson_foulds_distance.RobinsonFoulds(actual_newick, newick_format)))
