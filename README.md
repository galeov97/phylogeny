# Phylogeny Inferences using Minimum Spanning Trees
Created by Siddharth Annaldasula, Valentina Galeone, Johanna von Wachsmann, & Myrthe Willemsen.

Phylogeny Inference and Applications WS21/22

## Folders
`model_data` - 100 models downloaded from https://github.com/angtft/RAxMLGrove.

`output` - Output from simulating sequence evolution on each of the models at `100`,`1000`,`10000`, and `100000` bp.

`testing` - Testing conducting from testing various mst cutting algorithms and number of mst cuts created from running `testing.py`. (Note: This folder only contains some of the testing results. The rest can be found in `testing.zip`)

`results` - Results of the workflow from running `main.py`

## Functions

`tree.py` - `Vertex` and `Tree` classes and their corresponding functions.

`stepwiseaddition.py` - Constructing local trees using stepwise addition.

`merging.py` - Merging local trees.

`robison_foulds_distance.py` - Assessing quality of method with Robinson-Foulds distance.

`functions.py` - Common functions used across scripts.

## Configuration Files

`settings.py` - Settings to manipulate the workflow. Main setting is choosing which mst cutting method to use (one of "longest_branch","longest_path","downstream_nodes"). The rest of the parameters are to toggle plots during mst construction and partitioning.

`models.txt` - Model numbers that were downloaded from RAXML-NG

`sequence_lengths.txt` - Various sequence lengths that the models were simulated on.

`phylogeny.yml` - Conda environment file to run the corresponding scripts


## Standalone Scripts

`simulate_data.py` - Simulate the models in the `model_data` folder.

`testing.py` - Testing script. Given input is the model number, which is one from the `model_data` folder, and sequence length, which can be either `100`,`1000`,`10000`, and `100000` bp. Based on what mst `cut_type` is used, results will be generated under `testing/testing_${settings.mst_cut}/${model number}`.

`testing_visualizations.py` - Visualizes the results from `testing`.

`main.py` - Main script. See below on instructions on running.  Given input is the model number, which is one from the `model_data` folder, and sequence length, which can be either `100`,`1000`,`10000`, and `100000` bp. Output will be generated under `results`.

`results_visualizations.py` - Visualizes the results from the folder `results`.


## HPC Scripts

`CurtaBatchScript.sh` - Batch job submission script on the Curta HPC @ FU. Can call upon `main.py` or `testing.py`, calls upon those scripts using `models.txt` and `sequence_lengths.txt`. Example run on the command line `sbatch --array=0-100 CurtaBatchScript.sh`. To know how many jobs to run, multiply the number of models and the different sequence lengths that will be evaluated.


## Sample commands

`python testing.py 41824 100`

`python main.py 41824 1000`

`python testing_visualizations.py`

`python results_visualization.py`
