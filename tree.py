import numpy as np
import random
import scipy
import scipy.linalg
import re
from copy import deepcopy
from functions import *
from Bio import Phylo
import networkx as nx
import matplotlib.pyplot as plt
import math
from mst import *

class Vertex:
    def __init__(self,name):
        self.name = name
        self.parent = None
        self.children = [] # Use a list of "Vertex" to store children of the current vertex
        self.distanceToParent = 0 # "weight" from the parent to the current vertex
        self.sequence = ''
        self.neighbors = set() #the neighbors in the unrooted tree, keys are node name, values are distance
        self.timesVisited = 0 #for post order traversal
        self.LikelihoodVector = [] #ex for one site and a leaf: L = [0,1,0,0]

    def __str__(self):
        return self.name

    def CountInDegree(self):
        # Count the number of ancestors
        # Assumption - Rooted phylogenetic tree
        if (self.parent != None):
            return 1
        else:
            return 0

    def CountOutDegree(self):
        # Count the number of descendants
        return(len(self.children))

    def PrintChildren(self):
    # Prints the children of a vertex
        for child in self.children:
            print(child)

class Tree:
    def __init__(self, name):
        self.name = name
        self.nodes = {} # Dictionary of hidden nodes and leaves. The key is the name, the value the object
        self.edges = {}
        self.leaves = [] #name of leaves
        self.root = None
        self.verticesForPostOrderTraversal = [] #list of the names of the vertices for a post order traversal (leaves excluded)
        self.verticesForPreOrderTraversal = [] #list of the names of the vertices for a pre order traversal
        self.Q = np.mat([
          [-1, 1/3, 1/3, 1/3],
          [1/3, -1, 1/3, 1/3],
          [1/3, 1/3, -1, 1/3],
          [1/3, 1/3, 1/3, -1]])
        self.nucleotides = ["A", "C", "G", "T"]
        self.size = 1000 #length of the sequences to be evolved
        self.pi = [0.25]*4
        self.parsimony_score = 0
        self.undirected = False

    def __str__(self):
        # Prints the name of each vertex and its respective degree, in-degree, and out-degree in the tree
        output = "Tree name: %s\n" %(self.name)
        output += "Vertex\tIndegree\tOutdegree\tDegree\tDistance\n"
        for node in self.nodes:
            indegree = self.nodes[node].CountInDegree()
            outdegree = self.nodes[node].CountOutDegree()
            output += "%s\t%s\t%s\t%s\t%s\n" %(node,indegree,outdegree,indegree+outdegree,self.nodes[node].distanceToParent)
        return output

    def AddVertex(self, vertex):
        if vertex in self.nodes:
            print("Node %s is already in the tree" %vertex)
        else:
            self.nodes[vertex] = Vertex(vertex)

    def AddEdge(self, parent, child, distance=0):
        #modify the attributes of the nodes
        if not self.undirected:
            self.nodes[parent].children.append(child)
            self.nodes[child].parent = parent
            self.nodes[child].distanceToParent = distance

        if self.undirected:
            #create the undirected edges for the unrooted tree later:
            self.edges[parent, child] = distance
            self.nodes[parent].neighbors.add(child)
            self.nodes[child].neighbors.add(parent)

    def RemoveEdge(self, edge):
        #modify the attributes of the nodes
        #not tested with rooted
        if not self.undirected:
            parent, child = edge
            self.nodes[parent].children.remove(child)
            self.nodes[child].parent = None

        if self.undirected:
          self.edges.pop(edge)
          self.nodes[edge[0]].neighbors.remove(edge[1])
          self.nodes[edge[1]].neighbors.remove(edge[0])

    def ComputeRoot(self):
        # Computes and stores the root of the tree
        for node in self.nodes:
            if (self.nodes[node].CountInDegree() == 0):
                self.root = node
                return
    def HiddenNodes(self):
        hidden_nodes = []
        if self.undirected:
          for n in self.nodes:
            if len(self.nodes[n].neighbors) > 1:
              hidden_nodes.append(n)
        else:
          for n in self.nodes:
            if self.nodes[n].CountOutDegree() == 2:
              hidden_nodes.append(n)
        return hidden_nodes

    def pop_edge(self,node1,node2):
      try:
        return self.edges.pop((node1,node2))
      except:
        return self.edges.pop((node2,node1))

    def ComputeNewickFormatRecursive(self, node):
        # Helper function for ComputeNewickFormat
        current_node = self.nodes[node]
        if len(current_node.children) >= 1:
            return "(" + ",".join(map(self.ComputeNewickFormatRecursive, current_node.children)) + "):" + str(current_node.distanceToParent)
        else:
            return "%s:%s" %(current_node.name,current_node.distanceToParent)

    def ComputeNewickFormat(self):
        # Returns the newick format of the tree
        if self.root == None:
            self.ComputeRoot()
        node = self.nodes[self.root]
        return "(" + ",".join(map(self.ComputeNewickFormatRecursive, node.children)) + ");"

    def ReadNewickFile(self, newickFile="", path = "", undirected = False):
        #reads in a newick file, can read also a tree with more than two children
        if path != "":
            file = open(path, "r")
            newick_string = file.readline()
        else:
            newick_string = newickFile

        hidden_num = 1

        while re.search(r'\([^()]*\)', newick_string):
            # Reg ex is looking for the set of parenthesis with no inner parenthesis
            searching = re.search(r'\([^()]*\)', newick_string)
            subtree = searching.group(0)[1:][:-1]
            # Creating the current hidden node for the two vertex (can be a combination of leaves or hidden nodes)
            curr_hidden_node = "h" + str(hidden_num)
            hidden_num += 1
            self.AddVertex(curr_hidden_node)

            # Iterate through all the children of the current hidden node
            children = subtree.split(",")
            for child in children:
                name, distance = child.split(":")
                if name in self.nodes:
                    # Can directly add an edge if we have already seen the hidden node
                    self.AddEdge(curr_hidden_node,name,float(distance))
                else:
                    # Else we are at a leaf, so create a leaf and add it to the tree
                    self.AddVertex(name)
                    self.AddEdge(curr_hidden_node,name,float(distance))

            # Replace the string with the hidden node
            newick_string = newick_string.replace(searching.group(0), curr_hidden_node)

            # The root of the tree will always be the last hidden node added to the tree
        self.root = curr_hidden_node

    ## methods for post and pre order traversal
    def CleanTree(self):
        for n in self.nodes:
            self.nodes[n].Set = []
            self.nodes[n].timesVisited = 0

    def SetLeaves(self):
        #fills the leaves list
        self.leaves = []
        for node in self.nodes:
            if self.nodes[node].CountOutDegree() == 0:
                self.leaves.append(node)

    def SetVerticesForPostOrderTraversal(self, include_Leaves = False):
        self.CleanTree()
        #computes a post order traversal
        if len(self.leaves) == 0:
            self.SetLeaves()

        self.verticesForPostOrderTraversal = []

        verticesToVisit = deepcopy(self.leaves)
        while len(verticesToVisit) > 0:
            c = verticesToVisit.pop()
            p = self.nodes[c].parent
            self.nodes[p].timesVisited += 1
            if self.nodes[p].timesVisited == len(self.nodes[p].children): #originally= 2. i modified to len etc, cause it take into account multiple children. we should check though
                self.verticesForPostOrderTraversal.append(p)
                if self.nodes[p].CountInDegree() == 1:
                    verticesToVisit.append(p)
        if include_Leaves:#include the leaves in the list
            self.verticesForPostOrderTraversal = self.leaves + self.verticesForPostOrderTraversal

    def SetVerticesForPreOrderTraversal(self):
        self.SetVerticesForPostOrderTraversal(include_Leaves = True)
        num_vertices = len(self.nodes)
        for i in range(num_vertices):
            self.verticesForPreOrderTraversal.append(self.verticesForPostOrderTraversal[num_vertices-1-i])

    def EvolveSequences(self):
        self.SetVerticesForPreOrderTraversal()

        for node in self.verticesForPreOrderTraversal:

            if self.nodes[node].CountInDegree() == 0: #if root
                self.nodes[node].sequence = "".join([random.choices(self.nucleotides, weights=self.pi)[0] for _ in range(self.size)])
            else:
                t = self.nodes[node].distanceToParent
                P = scipy.linalg.expm(self.Q * t)
                parent = self.nodes[node].parent
                mutated_sequence = "".join([random.choices(self.nucleotides, weights= P[self.nucleotides.index(nucleotide)].tolist())[0] for nucleotide in self.nodes[parent].sequence])
                self.nodes[node].sequence = mutated_sequence

    def ComputeDescendants(self, node, descendants):
        if len(self.nodes[node].children) >= 1:
            for child in self.nodes[node].children:
                descendants.append(child)
                self.ComputeDescendants(child, descendants)
        return descendants

    def FindMRCA(self, current_node, node1, node2):
        for child in self.nodes[current_node].children:
            descendants = [child]
            descendants = self.ComputeDescendants(child, descendants)
            if bool(node1 in descendants) ^ bool(node2 in descendants): # exclusive or (XOR)
                MRCA = current_node
            elif (node1 in descendants) and (node2 in descendants):
                MRCA = self.FindMRCA(child, node1, node2)
        return MRCA

    def FindDistanceMRCA(self, node, MRCA): #MRCA needs to be ancestor of node1
        if (node == MRCA):
            return 0
        else:
            return self.nodes[node].distanceToParent + self.FindDistanceMRCA(self.nodes[node].parent, MRCA)

    def ComputeTreeDistance(self, node1, node2):
        MRCA = self.FindMRCA(self.root, node1, node2)
        return self.FindDistanceMRCA(node1,MRCA) + self.FindDistanceMRCA(node2,MRCA)

    def ComputemonyScoreIndex(self, index):
        all_sets = {}
        for leaf in self.leaves:
            all_sets[leaf] = {self.nodes[leaf].sequence[index]}

        for node in self.verticesForPostOrderTraversal:
            sets = [all_sets[child] for child in self.nodes[node].children]
            if (set.intersection(*sets)):
                all_sets[node] = set.intersection(*sets)
            else:
                self.mony_score += 1
                all_sets[node] = set.union(*sets)

    def ComputeParsimonyScoreIndex(self, index):
        all_sets = {}
        for leaf in self.leaves:
          all_sets[leaf] = {self.nodes[leaf].sequence[index]}

        for node in self.verticesForPostOrderTraversal:
          sets = [all_sets[child] for child in self.nodes[node].children]
          if (set.intersection(*sets)):
            all_sets[node] = set.intersection(*sets)
          else:
            self.parsimony_score += 1
            all_sets[node] = set.union(*sets)

    def ComputeParsimonyScore(self):
        self.SetLeaves()
        self.ComputeRoot()
        self.SetVerticesForPostOrderTraversal()
        self.parsimony_score = 0

        length = len(self.nodes[self.leaves[0]].sequence)
        for i in range(length):
            self.ComputeParsimonyScoreIndex(i)
        return self.parsimony_score

    def UnrootedDistance(self,parent,child):
            try:
                return self.edges[(parent,child)]
            except:
                return self.edges[(child,parent)]

    def FromRootedtoUnrooted(self):
        if (self.undirected == True):
            print ("tree already unrooted")
            return

        self.undirected = True

        if (len(self.nodes) == 1):
            self.root = None
            return

        self.ComputeRoot()

        del self.nodes[self.root] #remove the root from the set of nodes

        root_children = []
        for node in self.nodes:
            if (self.nodes[node].parent == self.root):
                root_children.append(node)
                self.nodes[node].parent = None
            else:
                self.edges[(node, self.nodes[node].parent)] = self.nodes[node].distanceToParent
                self.nodes[node].neighbors = {self.nodes[node].parent}

            for child in self.nodes[node].children:
                self.nodes[node].neighbors.add(child)

        #self.edges[(root_children[0],root_children[1])] = self.nodes[root_children[0]].distanceToParent + self.nodes[root_children[1]].distanceToParent
        self.edges[(root_children[0],root_children[1])]  = 1
        self.nodes[root_children[0]].neighbors.add(root_children[1])
        self.nodes[root_children[1]].neighbors.add(root_children[0])

        #still in testing mode, but makes sense to delete parent and children!
        for node in self.nodes:
            self.nodes[node].parent = None
            self.nodes[node].children = []

        return

    def toRootedHelper(self, parent):
        for child in self.nodes[parent].neighbors:

            # Add edge between parent and child
            self.AddEdge(parent, child, self.UnrootedDistance(parent,child))
            # Want to remove the directed edge between child to parent
            self.nodes[child].neighbors.remove(parent)
            self.toRootedHelper(child)

    def FromUnrootedtoRooted(self, rooted_edge = None, root_name = "root", distance_from_u = 0):
        if (self.undirected == False):
            print("tree already rooted")
            return

        self.undirected = False

        if (len(self.nodes) == 1):
            self.root = list(self.nodes)[0]
            return

        for node in self.nodes:
            self.nodes[node].children = []

        self.AddVertex(root_name)

        # Randomly select the edge to root, u -- v => u -- root -- v, and add to tree
        # One of root -- u or root -- v has to be 0, other is distance u -- v
        if (rooted_edge == None):
            rooted_edge = list(self.edges.keys())[random.randint(0,len(self.edges)-1)]
        u,v = rooted_edge[0],rooted_edge[1]
        self.root = root_name
        # if (self.UnrootedDistance(u,v) != 1):
        #     print(u,v)
        #     print(self.UnrootedDistance(u,v))
        self.AddEdge(root_name, u, distance_from_u)
        self.AddEdge(root_name, v, self.UnrootedDistance(u,v) - distance_from_u)
        #self.AddEdge(root_name, u, 1)
        #self.AddEdge(root_name, v, 1)

        # u and v are not connected anymore
        self.nodes[u].neighbors.remove(v)
        self.nodes[v].neighbors.remove(u)

        self.toRootedHelper(u) #recursively restoring directions
        self.toRootedHelper(v)

        #clear neighbours
        for node in self.nodes:
            self.nodes[node].neighbors = set()
        #clear edges
        self.edges = {}

        return

    def HiddenNodes(self):
        hidden_nodes = []
        if self.undirected:
            for n in self.nodes:
                if len(self.nodes[n].neighbors) > 1:
                    hidden_nodes.append(n)
        else:
            for n in self.nodes:
                if self.nodes[n].CountOutDegree() == 2:
                    hidden_nodes.append(n)
        return hidden_nodes

    def pop_edge(self,node1,node2):
            try:
                return self.edges.pop((node1,node2))
            except:
                return self.edges.pop((node2,node1))

    def PerformNNI(self, u, v):
        rooted = False

        if self.undirected == False:
          rooted = True
          print ("rooting")

        #Tree 1
        t1 = deepcopy(self)

        if rooted:
          t1.FromRootedtoUnrooted()

        # Take a,b child/branches from u & c,d branches from v
        t1.nodes[u].neighbors.remove(v)
        t1.nodes[v].neighbors.remove(u)
        a,b = list(t1.nodes[u].neighbors)[0],list(t1.nodes[u].neighbors)[1]
        c,d = list(t1.nodes[v].neighbors)[0],list(t1.nodes[v].neighbors)[1]

        # Swap b and c
        t1.nodes[u].neighbors = {a,c,v}
        t1.nodes[v].neighbors = {b,d,u}

        # Update the vertices
        t1.nodes[c].neighbors.add(u)
        t1.nodes[b].neighbors.add(v)
        t1.nodes[c].neighbors.remove(v)
        t1.nodes[b].neighbors.remove(u)

        # Update the edges
        t1.edges[(u,c)] = t1.UnrootedDistance(v,c)
        t1.edges[(v,b)] = t1.UnrootedDistance(u,b)
        t1.pop_edge(b,u)
        t1.pop_edge(c,v)


        if rooted:
          t1.FromUnrootedtoRooted()

        #Tree 2
        t2 = deepcopy(self)

        if rooted:
            t2.FromRootedtoUnrooted()

        # Take a,b child/branches from u & c,d branches from v
        t2.nodes[u].neighbors.remove(v)
        t2.nodes[v].neighbors.remove(u)
        a,b = list(t2.nodes[u].neighbors)[0],list(t2.nodes[u].neighbors)[1]
        c,d = list(t2.nodes[v].neighbors)[0],list(t2.nodes[v].neighbors)[1]

        # Swap b and c
        t2.nodes[u].neighbors = {a,d,v}
        t2.nodes[v].neighbors = {b,c,u}

        # Update the vertices
        t2.nodes[d].neighbors.add(u)
        t2.nodes[b].neighbors.add(v)
        t2.nodes[d].neighbors.remove(v)
        t2.nodes[b].neighbors.remove(u)

        # Update the edges

        t2.edges[(u,d)] = t2.UnrootedDistance(v,d)
        t2.edges[(v,b)] = t2.UnrootedDistance(u,b)
        t2.pop_edge(b,u)
        t2.pop_edge(d,v)
        if rooted:
          t2.FromUnrootedtoRooted()
        return t1, t2

    # likelihood and optimization functions
    def GetStationaryDistribution(self):
        rate_matrix_ = np.append(self.Q.T, [[1,1,1,1]], axis=0)
        stationary_distribution = np.linalg.lstsq(rate_matrix_, [0,0,0,0,1], rcond=None)[0]
        return stationary_distribution

    def SetSize(self):
        self.SetLeaves()
        self.size = len(self.nodes[self.leaves[0]].sequence)

    def RoundsNNI_MP(self, score, plot_nni = []):

        def SearchNextEdge(current_node, computed_nodes):
            for node1, node2 in self.edges:
                if (current_node == node1) and (node2 in computed_nodes):
                    return node2
                elif (current_node == node2) and (node1 in computed_nodes):
                    return node1
            return False

        max_rounds = int(math.log2(len(self.nodes)) + 1)

        n_rounds = 0
        for round_nni in range(0, max_rounds):
            print("\t\tNNI round: %d" %round_nni)

            hidden_nodes = self.HiddenNodes()
            if (len(hidden_nodes) < 2):
                return self
            u = hidden_nodes[0]
            hidden_nodes.remove(u)
            computed_nodes = [u]

            while (len(hidden_nodes) > 0):
                i = 0
                found = False
                while not found:
                    u = hidden_nodes[i]
                    found = SearchNextEdge(u, computed_nodes)
                    i += 1
                v = found
                t1, t2 = self.PerformNNI(u, v)
               
                self.FromUnrootedtoRooted()
                t1.FromUnrootedtoRooted()
                t2.FromUnrootedtoRooted()

                connectivity_MP = {connectivity: connectivity.ComputeParsimonyScore() for connectivity in [self, t1, t2]}

                self = min(connectivity_MP, key=connectivity_MP.get) # accept the new tree with the best likelihood.

                new_score = self.ComputeParsimonyScore()

                if settings.PLOT_NNI: plot_nni.append(new_score)

                self.FromRootedtoUnrooted()
                hidden_nodes.remove(u)
                computed_nodes.append(u)
            n_rounds += 1
            if new_score == score:
                break
            else:
                score = new_score

        if settings.PLOT_NNI:
            y = plot_nni
            print
            x = np.array(range(0, len(y))) * (n_rounds / len(y) -1 )

            plt.plot(x,y, c="black", marker ="." )
            plt.xlim(0, n_rounds)
            plt.ylabel("MP estimate")
            plt.xlabel("Number of NNI rounds")
            plt.show()

        return self


    def Visualize(self, plot=True, console=False, file_name=None): # only rooted
        """  Display the final tree
    Input:      Tree
    Output:     Newick visualized tree
        """
        newick_string = self.ComputeNewickFormat()
        with open("results/intermediate_newick_output.txt", "w") as file_object:
            file_object.write(newick_string)
        newick_object = Phylo.read("results/intermediate_newick_output.txt", "newick")
        if console:
                Phylo.draw_ascii(newick_object)
        if plot:
                Phylo.draw(newick_object)

    def VisualizeUnrooted(self): # only unrooted
        G = nx.Graph()
        for node in self.nodes:
            G.add_node(node)
        for edge in self.edges:
            G.add_edge(edge[0], edge[1], length = self.edges[edge])
        nx.draw(G, with_labels=True)
        plt.show()

    def MidPointRooting(self, distance_matrix=None, seq_dict=None):
        """ Uses heuristic Midpoint rooting to root the tree. """
        if not distance_matrix:
          seq_list = list(seq_dict.values())
          distance_matrix = JC_distance_matrix(seq_list)
        index1, index2 = np.unravel_index(np.argmax(distance_matrix, axis=None), distance_matrix.shape)
        node1, node2 = list(seq_dict.keys())[index1], list(seq_dict.keys())[index2]

        # randomly root tree
        if (self.undirected == True):
            self.FromUnrootedtoRooted()

        # Find path from u to v; mrca

        MRCA = self.FindMRCA(self.root, node1, node2)
        node1_to_MRCA = self.FindDistanceMRCA(node1, MRCA)
        node2_to_MRCA = self.FindDistanceMRCA(node2, MRCA)
        node1_to_node2 = node2_to_MRCA + node1_to_MRCA
        if node1_to_MRCA > node2_to_MRCA:
          node = node1
        else:
          node = node2
        distance = self.nodes[node].distanceToParent
        while distance < 0.5*node1_to_node2:
          node = self.nodes[node].parent
          distance += self.nodes[node].distanceToParent

        rooted_edge = (self.nodes[node].parent, node)
        
        if ("root" in rooted_edge): # already rooted at the midpoint in this case. Define better root here!
            return 
        
        self.FromRootedtoUnrooted()

        self.FromUnrootedtoRooted(rooted_edge=(rooted_edge), distance_from_u=distance - 0.5*node1_to_node2) #edge = u, v
        #self.Visualize()
