import numpy as np
from functions import *
import copy

from tree import Tree, Vertex
from mst import *
import math
import dendropy
from dendropy.interop import seqgen
import os
from io import StringIO
from Bio import SeqIO
import subprocess
import shlex
import sys

def evolve_trees(output_path, input, length):
    input_dir = len(next(os.walk(input_base_dir))[1])
    total_files = input_dir * len(length)
    count = 0
    print('Generating sequences: \n')
    for dir in os.listdir(input):
    # for dir in ['424']:
        my_tree = '/'.join([input, dir, 'tree_best.newick'])
        log = '/'.join([input, dir, 'log_0.txt'])
        with open(log) as log_file:
            lines = log_file.readlines()

        # get base frequencies
        base_frequencies = lines[8].split(' ')[3:7]
        # change to float and strip new-line character
        base_frequencies = [item.strip() for item in base_frequencies]
        # print(base_frequencies)
        # get substituatio rates
        substituation_rates = lines[9].split(' ')[3:9]
        # change to float and strip new-line character
        substituation_rates = [item.strip() for item in substituation_rates]
        # print(substituation_rates)
        model = lines[1].split(' ')[1].split('+')[0]

        #create dir for each of the 100 tree model sets
        for myleng in length:
            count += 1
            output = '/'.join([output_path, dir])
            if not os.path.exists(output):
                os.makedirs(output)

            # is -f base frequencies and -r substituation rates?
            file_name = '_'.join([dir, str(myleng)]) + '.fasta'
            output = '/'.join([output, file_name])

            f = open(output, 'w')
            call = ' '.join(['seq-gen', '-m', str(model), '-l', str(myleng), '-f', str(', '.join(base_frequencies)), '-r', ', '.join(substituation_rates), '-z', str(42), '-q', '-of', my_tree])
            cmd = shlex.split(call)
            subprocess.call(cmd, stdout=f)

            progress(count, total_files)


# PROGRSS BAR TAKEN FROM https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
def progress(count, total, status='', bar_len=60):
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    fmt = '[%s] %s%s ...%s' % (bar, percents, '%', status)
    print('\b' * len(fmt), end='')  # clears the line
    sys.stdout.write(fmt)
    sys.stdout.flush()

def get_trees(input, out, length):
    print('\n')
    print('Reading Tree Sequences')
    for dir in os.listdir(input):
        for i in length:
            name = '_'.join(['tree', str(dir), str(i)])
            tree = Tree(name)
            my_tree = '/'.join(['model_data', str(dir), 'tree_best.newick'])
            tree.ReadNewickFile(path=my_tree)
            file = str(dir) + '/' + str(dir) + '_' + str(i) + '.fasta'
            # print('file', file)
            fastafile = out + '/' + file
            for record in SeqIO.parse(fastafile, 'fasta'):
                if (tree.nodes[record.id].CountOutDegree() == 0):
                    tree.nodes[record.id].sequence = record.seq

if __name__ == '__main__':
    output_path = 'output'
    input_base_dir = 'model_data'
    # check if output dir exists otherwise creates such dir
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    length = [100, 1000, 10000, 100000]

    #check if input it a directory
    if os.path.isdir(input_base_dir):
        evolve_trees(output_path, input_base_dir, length)
    elif os.path.isfile(input_base_dir):
        print('Input path is a file instead of a directory')
    else:
        print("Input path is a special file (socket, FIFO, device file) and not a directory" )

    get_trees(input_base_dir, output_path, length)
    print('done!')
