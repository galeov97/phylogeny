from tree import Vertex, Tree
import numpy as np
from functions import *
import copy
import random
import itertools

def StepWiseAddition_MP(seq_dict):
    """Do stepwise addition.
    """
    def AddNextNode(new_tree, node, hidden_node, next_vertex):
        parent = new_tree.nodes[node].parent
        new_tree.AddVertex(next_vertex)
        new_tree.nodes[next_vertex].sequence = seq_dict[next_vertex]
        new_tree.AddVertex(hidden_node)

        new_tree.nodes[node].parent = hidden_node
        new_tree.AddEdge(hidden_node, next_vertex, 1)
        new_tree.AddEdge(hidden_node, node, 1)
        new_tree.AddEdge(parent, hidden_node, 1)
        new_tree.nodes[parent].children.remove(node)
        return new_tree

    complete_distance_matrix = uncorrected_distance_matrix(list(seq_dict.values()))
    #print (complete_distance_matrix)
    N = range(0, complete_distance_matrix.shape[0])
    ranking = np.argsort(np.asarray([up_distance(i, complete_distance_matrix) for i in N]))
    ranked_nodes = [list(seq_dict.keys())[rank] for rank in ranking]

    tree = Tree(name="tree")

    if (len(ranked_nodes) == 1):
        node = ranked_nodes[0]
        tree.AddVertex(node)
        tree.nodes[node].sequence = seq_dict[node]
        return tree

    if (len(ranked_nodes) == 2):
        root = "h1"
        tree.AddVertex(root)
        for i in range(2):
            node = ranked_nodes[i]
            tree.AddVertex(node)
            tree.nodes[node].sequence = seq_dict[node]
            tree.AddEdge(root, node, 1)
        return tree

    tree.undirected = True
    hidden_vertex = "h2"
    tree.AddVertex(hidden_vertex)

    for i in range(3):
        node = ranked_nodes[i]
        tree.AddVertex(node)
        tree.nodes[node].sequence = seq_dict[node]
        tree.AddEdge(node, hidden_vertex, 1)

    tree.FromUnrootedtoRooted(rooted_edge = (hidden_vertex,ranked_nodes[0]), root_name = "h1")

    for i in range(3,len(ranked_nodes)):
        hidden_node = "h" + str(i)
        next_vertex = ranked_nodes[i]
        all_options = {}
        for node in tree.nodes:
            if node != tree.root: #not root
                new_tree = copy.deepcopy(tree)
                new_tree = AddNextNode(new_tree, node, hidden_node, next_vertex)
                all_options[node] = new_tree.ComputeParsimonyScore()

        best_node = min(all_options, key=lambda k: all_options[k], default='')
        tree = AddNextNode(tree, best_node, hidden_node, next_vertex)
    #print(tree)
    return tree

if __name__ == '__main__':
    '''
    l1 ='ATGATATGGGATACACCCCAGTTTTACGCGGGCATTACACATGATATGGGATA'
    l2= 'AAGTTATGGGATACACCCCAGTTTTACGCGGGCATTACACATGATATGGGATA'
    l3= 'AAGATATGCGTTACACTCCAGTATTACGCGGGCATTACACATGATATGGGATA'
    l4= 'AAGCTACGAGTTACGCCTCAGTTATACGAGGGTATTACACATGATATGGGATA'
    l5= 'AAGCTACGAGTTACGCCTCAGTTATACGAGGGCACTACACATGATATGGGATA'
    l6= 'AAGCTAGGAGTTACGCCTCAGTTATACGAGGGTATTACACATGATATGGGATA'
    l7= 'AAGCTACGAGTTACGCCTCAGATATACCAGGGCACTACACATGATATGGGATA'
    seq_list = [l1,l2,l4, l3,l5, l6, l7]
    seq_identifiers = ['l1','l2','l4', 'l3','l5', 'l6', 'l7'] # sequence names, should not be duplicates.
    seq_dict = {seq_id:seq for seq_id, seq in zip(seq_identifiers, seq_list)}
    #tree = StepWiseAddition_new(seq_dict)
    '''
    nucleotides = ["A", "C", "G", "T"]
    pi = [0.25]*4
    size = 500
    seq_dict = {}
    for i in range(1,8):
        random.seed(i + 7)
        sequence = "".join([random.choices(nucleotides, weights=pi)[0] for _ in range(size)])
        seq_dict["l" + str(i)] = sequence

    tree = StepWiseAddition_MP(seq_dict)

    print(tree)
    tree.Visualize()
    tree.FromRootedtoUnrooted()

    tree = tree.RoundsNNI_MP()
    tree.FromUnrootedtoRooted()

    #tree.Visualize()
