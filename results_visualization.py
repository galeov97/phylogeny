import os
import json
import settings
import robinson_foulds_distance
import matplotlib.pyplot as plt


class EstimatedTree:
    def __init__(self,model,length,taxons,rf,ed,avg_br,avg_br_T1_T2,avg_br_T1,avg_depth_T1_T2,avg_depth_T1):
        self.model = model
        self.length = length
        self.taxons = taxons
        self.rf = rf
        self.ed = ed
        self.avg_br = avg_br
        self.avg_br_T1_T2 = avg_br_T1_T2
        self.avg_br_T1 = avg_br_T1
        self.avg_depth_T1_T2 = avg_depth_T1_T2
        self.avg_depth_T1 = avg_depth_T1


def GetNumberOfTaxons(input_dir, model):
    jsonfile = input_dir + '/' + model + '/' + 'tree_dict.json'
    f = open(jsonfile)
    data = json.load(f)
    f.close()
    return data[0]['NUM_TAXA'],data[0]['BRANCH_LENGTH_MEAN']

def CreateDataDict(input_dir):
    data = dict()

    for model in os.listdir("results"):
        with open(os.path.join(input_dir, model, 'tree_best.newick'), "r") as file:
            actual_newick = file.readline()
        taxons,avg_length = GetNumberOfTaxons(input_dir, model)

        for length_file in os.listdir(os.path.join("results",model)):
            length = length_file.split(".")[0]
            with open(os.path.join("results", model, length_file), "r") as file:
                estimated_newick = file.readline()

            rf = robinson_foulds_distance.RobinsonFoulds(actual_newick, estimated_newick)
            ed = robinson_foulds_distance.EuclidianDistance(actual_newick, estimated_newick)

            avg_br_T1_T2, avg_br_T1, avg_depth_T1_T2, avg_depth_T1 = robinson_foulds_distance.MissingBranches(actual_newick, estimated_newick)

            if model not in data:
                data[model] = {int(length):EstimatedTree(model,int(length),taxons,rf,ed,avg_length,avg_br_T1_T2, avg_br_T1, avg_depth_T1_T2, avg_depth_T1)}
            else:
                data[model][int(length)] = EstimatedTree(model,int(length),taxons,rf,ed,avg_length,avg_br_T1_T2, avg_br_T1, avg_depth_T1_T2, avg_depth_T1)
    return data

def RFChangeOverSequenceLengthVisualization(data):
    x = []
    rfs = []
    color_dict = {100:"black",1000:"blue",10000:"orange",100000:"red"}
    colors = []
    results100 = dict()
    results1000 = dict()
    results10000 = dict()
    results100000 = dict()

    for model in data:
        try:
            rf_100 = data[model][100].rf
            if (len(data[model]) == 4):
                x = [model for length in data[model]]
                rfs = [data[model][length].rf for length in data[model]]
                results100[data[model][100].taxons] = data[model][100].rf
                results1000[data[model][1000].taxons] = data[model][1000].rf
                results10000[data[model][10000].taxons] = data[model][10000].rf
                results100000[data[model][100000].taxons] = data[model][100000].rf
        except:
            pass
    results_list = list(results100)
    results_list.sort()
    fig, ax = plt.subplots()
    z = []
    for length in [100000,10000,1000,100]:
        x = []
        y = []
        for i in results_list:
            x.append(i)
            if (length == 100): y.append(results100[i])
            if (length == 1000): y.append(results1000[i])
            if (length == 10000): y.append(results10000[i])
            if (length == 100000): y.append(results100000[i])
        if (z == []):
            ax.bar(x,y,label = length)
            z = y
        else:
            ax.bar(x,y,label = length, bottom = z)
            z = [z[j] + y[j] for j in range(len(y))]
    ax.legend(title = "Sequence Length")
    plt.xlabel("Number of Taxa")
    plt.ylabel("Robinson Fould Distance")
    plt.show()


def EDChangeOverSequenceLengthVisualization(data):
    x = []
    eds = []
    color_dict = {100:"black",1000:"blue",10000:"orange",100000:"red"}
    colors = []
    for model in data:
        ed_100 = data[model][100].ed
        x += [data[model][length].model for length in data[model]]
        eds += [data[model][length].ed - ed_100 for length in data[model]]
        colors += [color_dict[data[model][length].length] for length in data[model]]

    plt.scatter(x,eds,c=colors)
    plt.show()


def AvgBRLengthChangeOverSequenceLengthVisualization(data):
    x = []
    y = []
    color_dict = {100:"black",1000:"blue",10000:"orange",100000:"red"}
    colors = []
    for model in data:
        avg_br_100 = data[model][100].avg_br
        avg_br_T1_T2_100 = data[model][100].avg_br_T1_T2
        avg_br_T1_100 = data[model][100].avg_br_T1
        x.append(avg_br_T1_T2_100 - avg_br_100)
        y.append(avg_br_T1_100 - avg_br_100)
    plt.scatter(x,y)
    plt.xlabel("Edges belonging to T1 & T2")
    plt.ylabel("Edges belonging to only T1")
    plt.show()

def AvgBRDepthChangeOverSequenceLengthVisualization(data):
    x = []
    y = []
    color_dict = {100:"black",1000:"blue",10000:"orange",100000:"red"}
    colors = []
    for model in data:
        avg_depth_T1_T2_100 = data[model][100].avg_depth_T1_T2
        avg_depth_T1_100 = data[model][100].avg_depth_T1
        x.append(avg_depth_T1_T2_100)
        y.append(avg_depth_T1_100)
    plt.scatter(x,y)
    plt.xlabel("Edges belonging to T1 & T2")
    plt.ylabel("Edges belonging to only T1")
    plt.show()

if __name__ == '__main__':
    settings.init()
    input_dir = 'model_data'

    data = CreateDataDict(input_dir)
    RFChangeOverSequenceLengthVisualization(data)
    #AvgBRLengthChangeOverSequenceLengthVisualization(data)
    #AvgBRDepthChangeOverSequenceLengthVisualization(data)
