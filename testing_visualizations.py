import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
import json
import settings
import robinson_foulds_distance
import numpy as np

def GetNumberOfTaxons(input_dir, model):
    jsonfile = input_dir + '/' + model + '/' + 'tree_dict.json'
    f = open(jsonfile)
    data = json.load(f)
    f.close()
    return data[0]['NUM_TAXA'],data[0]['BRANCH_LENGTH_MEAN']

def PlotModelTestingAcrossCutsRF(model, length = 100):
    for cut_type in ["longest_branch","longest_path","downstream_nodes"]:
        if (os.path.exists(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))):
            df = pd.read_csv(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))
            sns.lineplot(x = [i for i in range(len(list(df.columns)))], y = list(df.iloc[0]), label = cut_type)
    plt.xlabel("Number of Cuts")
    plt.ylabel("Robinson Fould")
    taxon,_ = GetNumberOfTaxons('model_data',model)
    plt.title("Model: %s, # of Taxa: %s, Sequence Length: %d" %(model,taxon,length))
    plt.show()

def PlotModelTestingAcrossCutsRF3D(model, length = 100):
    taxon,_ = GetNumberOfTaxons('model_data',model)
    ax = plt.axes(projection="3d")
    for cut_type in ["longest_branch"]:#,"longest_path","downstream_nodes"]:
        if (os.path.exists(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))):
            df = pd.read_csv(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))
            print(df)
            x = np.array([i for i in range(len(list(df.columns)))])
            y = np.array(list(df.iloc[0]))
            z = np.array([taxon for _ in range(len(list(df.columns)))])
            ax.plot_surface(X = y, Y = z, label = cut_type, Z = x)
    plt.xlabel("Number of Cuts")
    plt.ylabel("Robinson Fould")
    plt.title("%s,%s,%s" %(model,taxon,length))
    plt.show()

def PlotModelTestingAcrossCutsED(model, length = 100):
    with open(os.path.join(input_dir, model, 'tree_best.newick'), "r") as file:
        actual_newick = file.readline()

    for cut_type in ["longest_branch","longest_path","downstream_nodes"]:
        eds = []
        cuts = []
        if(os.path.exists(os.path.join("testing","testing_%s" %cut_type,model,str(length)))):
            for cut in os.listdir(os.path.join("testing","testing_%s" %cut_type,model,str(length))):
                with open(os.path.join("testing", "testing_%s" %cut_type, model, str(length), cut), "r") as file:
                    estimated_newick = file.readline()
                eds.append(robinson_foulds_distance.EuclidianDistance(actual_newick, estimated_newick))
                cuts.append(int(cut.split(".")[0]))
            sns.lineplot(x = cuts, y = eds, label = cut_type)

    plt.xlabel("Number of Cuts")
    plt.ylabel("Euclidian Distance")
    taxon,branch_length = GetNumberOfTaxons('model_data',model)
    plt.title("%s,%s,%s" %(model,taxon,branch_length))
    plt.show()

def PlotBestCut(cut_type,length = 100):
    lengths = []
    taxons = []
    cuts = []
    for model in os.listdir(os.path.join("testing","testing_%s" %cut_type)):
        if (os.path.exists(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))):
            df = pd.read_csv(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))
            #rf = list(df.iloc[0])[list(df.iloc[0]).index(min(list(df.iloc[0])))]
            rfs = list(df.iloc[0])[1:]
            if (len(rfs) > 1):
                cut = rfs.index(min(rfs)) + 1
                cuts.append(cut)

                taxon,branch_length = GetNumberOfTaxons('model_data',model)
                taxons.append(taxon)
                lengths.append(branch_length)

    sns.scatterplot(x = taxons, y = cuts)
    plt.xlabel("Number of Taxa")
    plt.ylabel("Number of Cuts")
    plt.title("MST Cut Type: %s, Sequence Length: %s" %(cut_type,length))
    plt.show()

    #sns.scatterplot(x = lengths, y = cuts)
    #plt.xlabel("Average Branch Length")
    #plt.ylabel("Cuts")
    #plt.title("%s,%s" %(cut_type,length))
    #plt.show()

def PlotBestCutAll(cut_type,length = 100):
    lengths = []
    taxons = []
    avg_rfs = {0:[],1:[],2:[],3:[],4:[],5:[],6:[],7:[],8:[],9:[],10:[]}
    for model in os.listdir(os.path.join("testing","testing_%s" %cut_type)):
        if (os.path.exists(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))):
            df = pd.read_csv(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))
            rfs = list(df.iloc[0])
            if (len(rfs) > 1):
                min_rf = rfs[0]
                rfs = [rf - min_rf for rf in rfs]

                taxon,branch_length = GetNumberOfTaxons('model_data',model)
                taxons.append(taxon)
                lengths.append(branch_length)

                sns.lineplot(y = rfs, x = [i for i in range(len(rfs))])
                for rf in range(len(rfs)):
                    avg_rfs[rf].append(rfs[rf])
    y = [0,0,0,0,0,0,0,0,0,0,0]
    for rf in avg_rfs:
        y[rf] = np.mean(avg_rfs[rf])
    sns.lineplot(y = y, x = [i for i in range(len(avg_rfs))], color='black', linewidth=4)
    plt.ylabel("Robinson Fould Change")
    plt.xlabel("Number of Cuts")
    plt.title("MST Cut Type: %s, Sequence Length: %s" %(cut_type,length))
    plt.show()

    #sns.scatterplot(x = lengths, y = cuts)
    #plt.xlabel("Average Branch Length")
    #plt.ylabel("Cuts")
    #plt.title("%s,%s" %(cut_type,length))
    #plt.show()

def PlotBestCutAll3D(cut_type,length = 100):
    lengths = []
    taxons = []
    cuts = []
    #x = np.outer(np.linspace(0,900,901),np.ones(11))


    fig = plt.figure()
    ax = plt.axes(projection="3d")

    for model in os.listdir(os.path.join("testing","testing_%s" %cut_type)):
        x = []
        y = []
        z = []
        if (os.path.exists(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))):
            df = pd.read_csv(os.path.join("testing","testing_%s" %cut_type,model,"%s_%d.txt" %(model,length)))
            #rf = list(df.iloc[0])[list(df.iloc[0]).index(min(list(df.iloc[0])))]
            rfs = list(df.iloc[0])
            min_rf = rfs[0]
            rfs = [rf - min_rf for rf in rfs]
            taxon,_ = GetNumberOfTaxons('model_data',model)
            for cut in range(len(rfs)):
                x.append(taxon)
                y.append(cut)
                z.append(rfs[cut])
            X = np.array(x)
            Y = np.array(y)
            Z = np.array(z)

            ax.plot(X, Y, Z)

    ax.set_zlim(-5, 10)
    plt.xlabel("Taxon")
    plt.ylabel("Number of Cuts")

    plt.title("%s,%s" %(cut_type,length))
    plt.show()


if __name__ == '__main__':
    settings.init()
    input_dir = 'model_data'


    #for model in os.listdir("testing/testing_downstream_nodes"):
    #    model = "36002"
    #    for length in [100,1000,10000,100000]:
    #        PlotModelTestingAcrossCutsRF(model,length)
    #    assert(1==0)

    for mst_cut in ["longest_branch","longest_path","downstream_nodes"]:
        for length in [100,1000,10000,100000]:
            #PlotBestCut(mst_cut,length)
            PlotBestCutAll(mst_cut,length)
        #PlotBestCutAll3D(mst_cut,100)
        #PlotBestCut(settings.mst_cut,100)
