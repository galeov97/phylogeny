import time,sys,copy,random,os,json
import matplotlib.pyplot as plt
import numpy as np
from Bio import SeqIO

import settings
from tree import Tree, Vertex
import mst
import math
import stepwiseaddition
import merging
import robinson_foulds_distance

def create_phylogenetic_tree_n(seq_dict, n):
	times = []
	print("MST Starting...")
	start = time.time()
	super_set = mst.get_super_set(seq_dict, n)
	times.append(time.time() - start)
	print("...MST Finishing")
	forest = set() # s/o forest gump for believing in us
	start = time.time()
	for sub_seq_dict in super_set:
	    print("\tStepwise Starting...")
	    sub_tree = stepwiseaddition.StepWiseAddition_MP(sub_seq_dict)
	    print("\t\tNNI Starting...")
	    PS_score = 0
	    if len(sub_tree.nodes) > 2:
	        PS_score = sub_tree.ComputeParsimonyScore()
	    sub_tree.FromRootedtoUnrooted()
	    if settings.PLOT_NNI and len(sub_tree.nodes) > 2:
	        plot_nni = [PS_score]
	        sub_tree = sub_tree.RoundsNNI_MP(PS_score, plot_nni)
	    else:
	        sub_tree = sub_tree.RoundsNNI_MP(PS_score)
	    print("\t\t...NNI Finishing")
	    #sub_tree.FromUnrootedtoRooted()
	    forest.add(sub_tree)
	    print("\t...Stepwise Finishing")
	times.append(time.time() - start)

	start = time.time()
	print("Merging Starting...")
	nr_cut = len(forest)
	final_tree = merging.merging(forest)
	print("...Merging Finishing")

	print("NNI Whole Tree Starting...")
	final_tree.FromUnrootedtoRooted()
	PS_score = final_tree.ComputeParsimonyScore()
	final_tree.FromRootedtoUnrooted()

	times.append(time.time() - start)
	if settings.PLOT_NNI:
	    plot_nni = [PS_score]
	    final_tree = final_tree.RoundsNNI_MP(PS_score, plot_nni)
	else:
	    final_tree = final_tree.RoundsNNI_MP(PS_score)
	final_tree.FromUnrootedtoRooted()
	final_tree.MidPointRooting(seq_dict=seq_dict)
	print("...NNI Whole Tree Finishing")
	print("In the end, it doesn't even matter.")
	newick_format = final_tree.ComputeNewickFormat()
	if settings.PLOT_TREE: final_tree.Visualize()

	return newick_format, nr_cut, times

def OneModel(dir, length):
    # directories
    input_dir = 'model_data'
    estimated_dir = 'model_data'
    output_dir = 'output'

    # get actual newick
    my_tree = '/'.join([input_dir, dir, 'tree_best.newick'])
    with open(my_tree, "r") as file:
        actual_newick = file.readline()

    log = '/'.join([input_dir, dir, 'log_0.txt'])
    with open(log) as log_file:
        lines = log_file.readlines()
    model = lines[1].split(' ')[1].split('+')[0]

    # get base frequencies
    base_frequencies = lines[8].split(' ')[3:7]
    base_frequencies = [item.strip() for item in base_frequencies]

    # get substituatiom rates
    substitution_rates = lines[9].split(' ')[3:9]
    substitution_rates = [float(item.strip()) for item in substitution_rates]
    average_substitution_rates = np.mean(substitution_rates)

    # get sequences
    fastafile = output_dir + '/' + str(dir) + '/' + str(dir) + '_' + str(length) + '.fasta'
    seq_dict = {rec.id: str(rec.seq) for rec in SeqIO.parse(fastafile, "fasta")}

    return (seq_dict,actual_newick)

def BestNumberOfCuts(model, length = 100):
	if (not os.path.isdir("testing")):
		os.mkdir("testing")

	if (not os.path.exists(os.path.join("testing","testing_%s" %settings.mst_cut,model,"%s_%d.txt" %(model,length)))):
		seq_dict,actual_newick = OneModel(model,length)
		nr_partitions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

		RF = []
		nr_cuts = []
		times_all = [[] for _ in range(3)]

		if (not os.path.isdir(os.path.join("testing","testing_%s" %(settings.mst_cut)))):
			os.mkdir(os.path.join("testing","testing_%s" %(settings.mst_cut)))

		if (not os.path.isdir(os.path.join("testing","testing_%s" %settings.mst_cut,"%s" %model))):
			os.mkdir(os.path.join("testing","testing_%s" %settings.mst_cut,"%s" %model))

		if (not os.path.isdir(os.path.join("testing","testing_%s" %settings.mst_cut,"%s" %model,str(length)))):
			os.mkdir(os.path.join("testing","testing_%s" %settings.mst_cut,"%s" %model, str(length)))

		for n in nr_partitions:
			newick_format, nr_cut, times = create_phylogenetic_tree_n(seq_dict, n)
			if nr_cut not in nr_cuts:
				RF_res = robinson_foulds_distance.RobinsonFoulds(actual_newick, newick_format)
				print("RF of model %s with %s partition(s): %d" % (model, nr_cut, RF_res))

				RF.append(str(RF_res))
				nr_cuts.append(str(nr_cut))
				for i in range(len(times_all)):
						times_all[i].append(times[i])

				# Just in case script finishes without completing
				with open(os.path.join("testing","testing_%s" %settings.mst_cut,"%s" %model, str(length),str(n) + ".txt"), "w+") as file:
					file.write(newick_format)

				filedata = open(os.path.join("testing","testing_%s" %settings.mst_cut,model,"%s_%d.txt" %(model,length)),"w+")
				filedata.writelines(",".join(nr_cuts))
				filedata.writelines("\n")
				filedata.writelines(",".join(RF))
				filedata.writelines("\n")
				for i in range(len(times_all)):
					filedata.writelines(",".join([str(i) for i in times_all[i]]))
					filedata.writelines("\n")
				filedata.close()
	else:
		print("Already Exists: Cut Type: %s, Model: %s, Sequence Length: %d" %(settings.mst_cut,model,length))

if __name__ == '__main__':
	settings.init()

	input_dir = 'model_data'
	model = sys.argv[1]
	length = int(sys.argv[2])

	BestNumberOfCuts(model, length)
