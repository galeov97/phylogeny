import dendropy
import math
from dendropy.calculate import treecompare
import matplotlib.pyplot as plt
import numpy as np
import settings
import main

def RobinsonFoulds(actual_tree, estimated_tree):
    """Using library dendropy https://github.com/Zsailer/DendroPy/blob/master/dendropy/calculate/treecompare.py"""
    tns = dendropy.TaxonNamespace()

    tree1 = dendropy.Tree.get(
        data=actual_tree,
        schema='newick',
        taxon_namespace=tns)
    tree2 = dendropy.Tree.get(
        data=estimated_tree,
        schema='newick',
        taxon_namespace=tns)

    return treecompare.unweighted_robinson_foulds_distance(tree1, tree2)

def EuclidianDistance(actual_tree, estimated_tree):
    """Using library dendropy https://github.com/Zsailer/DendroPy/blob/master/dendropy/calculate/treecompare.py"""
    tns = dendropy.TaxonNamespace()

    tree1 = dendropy.Tree.get(
        data=actual_tree,
        schema='newick',
        taxon_namespace=tns)
    tree2 = dendropy.Tree.get(
        data=estimated_tree,
        schema='newick',
        taxon_namespace=tns)

    return treecompare.euclidean_distance(tree1, tree2)

def MissingBranches(actual_tree, estimated_tree):
    """Using library dendropy https://github.com/Zsailer/DendroPy/blob/master/dendropy/calculate/treecompare.py"""
    tns = dendropy.TaxonNamespace()

    tree1 = dendropy.Tree.get(
        data=actual_tree,
        schema='newick',
        taxon_namespace=tns)
    tree2 = dendropy.Tree.get(
        data=estimated_tree,
        schema='newick',
        taxon_namespace=tns)


    missing_bipartitions = treecompare.find_missing_bipartitions(reference_tree=tree1, comparison_tree=tree2,
                                                             is_bipartitions_updated=False)

    # rank corresponds to depth: https://dendropy.readthedocs.io/en/main/primer/bipartitions.html?highlight=Bipartition#a-bipartition-is-a-partitioning-of-taxa-corresponding-to-an-edge-of-a-tree

    calc_depth = lambda bipartitions : [sum([int(i) for i in (str(bipartition))]) for bipartition in bipartitions]
    depth_missing  = calc_depth(missing_bipartitions)
    depths = calc_depth(tree1.encode_bipartitions())

    branch_lengths = treecompare._get_length_diffs(tree1, tree2)
    branches_in_T1_and_T2, branches_only_T1 = [], []
    for i in branch_lengths:
        if i[0] != 0 and i[1] != 0:
            branches_in_T1_and_T2.append(i[0])
        elif i[1] == 0 and i[0] != 0:
            branches_only_T1.append(i[0])

    average_branches_in_T1_and_T2 = sum(branches_in_T1_and_T2)/len(branches_in_T1_and_T2)
    if (len(branches_only_T1) == 0):
        average_branches_only_T1 = 0
    else:
        average_branches_only_T1 = sum(branches_only_T1)/len(branches_only_T1)
    #print('average branch length of branches found in T and T^', average_branches_in_T1_and_T2)
    #print('average branch length of branches only in T', average_branches_only_T1)

    for edge in tree1.postorder_edge_iter():
        if (edge.length != 0.0):
            edge.length = 1.0

    tree1.calc_node_ages(ultrametricity_precision = False)
    tree1.set_edge_lengths_from_node_ages()
    tree2.calc_node_ages(ultrametricity_precision = False)
    tree2.set_edge_lengths_from_node_ages()
    branch_lengths = treecompare._get_length_diffs(tree1, tree2)
    branch_depth_in_T1_and_T2, branch_depth_only_T1 = [], []
    for i in branch_lengths:
        if i[0] != 0 and i[1] != 0:
            branch_depth_in_T1_and_T2.append(i[0])
        elif i[1] == 0 and i[0] != 0:
            branch_depth_only_T1.append(i[0])
    average_branch_depth_in_T1_and_T2 = sum(branch_depth_in_T1_and_T2)/len(branch_depth_in_T1_and_T2)
    if (len(branch_depth_only_T1) == 0):
        average_branch_depth_only_T1 = 0
    else:    
        average_branch_depth_only_T1 = sum(branch_depth_only_T1)/len(branch_depth_only_T1)
    #print('average branch depth of branches found in T and T^', average_branch_depth_in_T1_and_T2)
    #print('average branch depth of branches only in T', average_branch_depth_only_T1)


    # # bins = np.histogram(np.hstack((depths, depth_missing)))[1]
    # # plt.hist(depths, color="lightgrey", bins=bins, label="actual tree")
    # # plt.hist(depth_missing, color="grey",   label="missing in estimated tree", bins=bins)
    # # plt.legend(edgecolor="white")
    # # # plt.xlim(xmin=0, xmax=max(data)+0.1)
    # # plt.xlabel("Branch depths")
    # # plt.ylabel("Number of branches")
    # # # plt.show()
    return average_branches_in_T1_and_T2, average_branches_only_T1, average_branch_depth_in_T1_and_T2, average_branch_depth_only_T1


if __name__ == '__main__':
    settings.init()

    seq_dict,actual_newick = main.OneModel('66496',100)
    newick_format = main.create_phylogenetic_tree(seq_dict)
    print("RF:")
    print(RobinsonFoulds(actual_newick, newick_format))
    print('-----------------------------------------------------------')
    MissingBranches(actual_newick, newick_format)
