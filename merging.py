from mst import *
from stepwiseaddition import *

def merging(forest):
    new_trees = forest
    hv = "hv"
    count = 1

    while (len(new_trees) > 1):
        best_merge = None
        best_score = 0

        # Choose random trees
        tree1_og = new_trees.pop()
        tree2_og = new_trees.pop()

        if (len(tree2_og.nodes) > len(tree1_og.nodes)):
            tree1_og,tree2_og = tree2_og,tree1_og

        # Special case for tree with one nodes
        if (len(tree1_og.nodes) == 1):
            tree1 = copy.deepcopy(tree1_og)
            node1 = list(tree1.nodes)[0]
            node2 = list(tree2_og.nodes)[0]
            tree1.AddVertex(node2)
            tree1.nodes[node2].sequence = tree2_og.nodes[node2].sequence
            tree1.AddEdge(node1,node2,1)

            tree1_root = copy.deepcopy(tree1)
            tree1_root.FromUnrootedtoRooted()
            score = tree1_root.ComputeParsimonyScore()
            if ((best_merge == None) or (score < best_score)):
                best_score = score
                best_merge = tree1
        else:
            for edge1 in tree1_og.edges:
                # Special case for both trees with one nodes
                if (len(tree2_og.nodes) == 1):
                    tree1 = copy.deepcopy(tree1_og)
                    hv = "hv" + str(count)
                    count += 1

                    tree1.AddVertex(hv)
                    u1,v1 = edge1
                    tree1.RemoveEdge(edge1)
                    tree1.AddEdge(u1,hv,1)
                    tree1.AddEdge(v1,hv,1)

                    node2 = list(tree2_og.nodes)[0]
                    tree1.AddVertex(node2)
                    tree1.nodes[node2].sequence = tree2_og.nodes[node2].sequence
                    tree1.AddEdge(hv,node2,1)

                    tree1_root = copy.deepcopy(tree1)
                    tree1_root.FromUnrootedtoRooted()
                    score = tree1_root.ComputeParsimonyScore()
                    if ((best_merge == None) or (score < best_score)):
                        best_score = score
                        best_merge = tree1
                else:
                    for edge2 in tree2_og.edges:
                        tree1 = copy.deepcopy(tree1_og)
                        tree2 = copy.deepcopy(tree2_og)

                        hv1 = "hv" + str(count)
                        hv2 = "hv" + str(count+1)
                        count += 2

                        tree1.AddVertex(hv1)
                        tree2.AddVertex(hv2)

                        u1,v1 = edge1
                        tree1.RemoveEdge(edge1)
                        #del tree1.edges[edge1]
                        tree1.AddEdge(u1,hv1,1)
                        tree1.AddEdge(v1,hv1,1)

                        u2,v2 = edge2
                        tree2.RemoveEdge(edge2)
                        #del tree2.edges[edge2]
                        tree2.AddEdge(u2,hv2,1)
                        tree2.AddEdge(v2,hv2,1)

                        t2_t1 = dict()

                        # Add all vertices from tree2 to tree1
                        for node in tree2.nodes:
                            if (len(tree2.nodes[node].neighbors) > 1):
                                hv = "hv" + str(count)
                                count += 1
                                tree1.AddVertex(hv)
                                tree1.nodes[hv].sequence = tree2.nodes[node].sequence
                                t2_t1[node] = hv
                            else:
                                tree1.AddVertex(node)
                                tree1.nodes[node].sequence = tree2.nodes[node].sequence
                                t2_t1[node] = node

                        # Add all edges from tree2 to tree 1
                        for edge in tree2.edges:
                            tree1.AddEdge(t2_t1[edge[0]],t2_t1[edge[1]],1)
                        tree1.AddEdge(hv1,t2_t1[hv2],1)

                        tree1_root = copy.deepcopy(tree1)
                        tree1_root.FromUnrootedtoRooted()
                        score = tree1_root.ComputeParsimonyScore()
                        if ((best_merge == None) or (score < best_score)):
                            best_score = score
                            best_merge = tree1

        new_trees.add(best_merge)
    final_tree = new_trees.pop()
    return final_tree

if __name__ == '__main__':
    global PLOT
    PLOT = False
    nucleotides = ["A", "C", "G", "T"]
    pi = [0.25]*4
    size = 20
    seq_dict = {}
    seq_list = []
    for i in range(1,10):
        random.seed(i + 7)
        sequence = "".join([random.choices(nucleotides, weights=pi)[0] for _ in range(size)])
        seq_dict["l" + str(i)] = sequence
        seq_list.append(sequence)

    mst_matrix = get_mst(seq_list)
    forest, cut_vertices = cut_mst(mst_matrix, seq_dict)
    #draw_graph(mst_matrix, seq_dict)

    new_trees = set()
    for tree in forest:
        nodes = list(tree.nodes)
        nodes = [list(seq_dict.keys())[node] for node in nodes]
        sub_seq_dict = {node: seq_dict[node] for node in nodes}
        new_tree = StepWiseAddition_MP(sub_seq_dict)
        new_trees.add(new_tree)

    print("new stuffff")
    # Loop until all trees have been merged
    final_tree = merging(new_trees)

    final_tree.Visualize()
    print("The End")
