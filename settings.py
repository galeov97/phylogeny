def init():
	global PLOT_NETWORK
	global PLOT_DISTRIBUTION
	global PLOT_DEGREE
	global PLOT_NNI
	global PLOT_TREE
	global print_cut_info
	global mst_cut

	PLOT_NETWORK = False
	PLOT_DISTRIBUTION = False
	PLOT_DEGREE = False
	PLOT_NNI = False
	PLOT_TREE = False
	print_cut_info = False
	mst_cut = "downstream_nodes" #["longest_branch","longest_path","downstream_nodes"]
